package com.jspider.ManyToOne.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.jspider.ManyToOne.entity.Students;
import com.jspider.ManyToOne.entity.Teacher;
import com.jspider.ManyToOne.util.SingletonSessionProvider;

/**
 * @author Saptarshi Das
 *
 */
public class RepositoryClass {
  /**
 * @param save teacher method
 */
public static void saveTeacher(Teacher teacher) {
	  Session sess = SingletonSessionProvider.getSession();
	  Transaction tran = sess.beginTransaction();
	  sess.save(teacher);
	  tran.commit();
  }
  /**
 * @param save student method 
 */
public static void saveStudent(Students student) {
	  Session sess = SingletonSessionProvider.getSession();
	  Transaction tran = sess.beginTransaction();
	  sess.save(student);
	  tran.commit(); 
  }
}
