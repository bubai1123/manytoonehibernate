package com.jspider.ManyToOne.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.jspider.ManyToOne.constant.AppConstant;
/**
 * @author Saptarshi Das
 *
 */
@Entity
@Table(name=AppConstant.TEACHER_INFO)
public class Teacher implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "teacher_id")
	private int id;
	@Column(name = "teacher_name")
	private String name;
//	@OneToMany(cascade=CascadeType.ALL)
//	@JoinColumn(name="student_id")
	///private Students student;
	/**
	 * @param getId(Teacher) method
	 * @return Integer value
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param setId
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param getName(Teacher) method
	 * @return String value
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param setName
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Teacher [id=" + id + ", name=" + name  + "]";
	}
	
	
}
