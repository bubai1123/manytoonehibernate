package com.jspider.ManyToOne.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import com.jspider.ManyToOne.constant.AppConstant;
/**
 * @author Saptarshi Das
 *
 */
@Entity
@Table(name=AppConstant.STUDENT_INFO)
public class Students implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "student_id")
	private Long id;
	@Column(name="student_name")
	private String name;
	@Column(name="student_age")
	private int age;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="teacher_id")
	private Teacher teacher;
	/**
	 * @param getId(Student) method
	 * @return Long value
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param setId method 
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @param getName(Student) method
	 * @return String value
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**@param getAge(Student) method
	 * @return Integer value
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param setAge method
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @param getTeacher method
	 * @return Teacher object
	 */
	public Teacher getTeacher() {
		return teacher;
	}
	/**
	 * @param setTeacher method
	 */
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	@Override
	public String toString() {
		return "Students [id=" + id + ", name=" + name + ", age=" + age + ", teacher=" + teacher + "]";
	}

}
