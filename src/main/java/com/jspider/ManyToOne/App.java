package com.jspider.ManyToOne;

import java.util.ArrayList;
import java.util.List;

import com.jspider.ManyToOne.entity.Students;
import com.jspider.ManyToOne.entity.Teacher;
import com.jspider.ManyToOne.repository.RepositoryClass;

/**
 * Hello world!
 *
 */
/**
 * @author Saptarshi Das
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Hello World!");
		Teacher teacher = new Teacher();
		Students stud1 = new Students();
		stud1.setName("Gourahari");
		stud1.setAge(21);
		stud1.setTeacher(teacher);

		RepositoryClass.saveStudent(stud1);

		Students stud2 = new Students();
		stud2.setName("Dipti");
		stud2.setAge(21);
		stud2.setTeacher(teacher);

		RepositoryClass.saveStudent(stud2);

		Students stud3 = new Students();
		stud3.setName("Saptarshi");
		stud3.setAge(28);
		stud3.setTeacher(teacher);

		RepositoryClass.saveStudent(stud3);

		Students stud4 = new Students();
		stud4.setName("Pawan");
		stud4.setAge(20);
		stud4.setTeacher(teacher);

		RepositoryClass.saveStudent(stud4);

		Students stud5 = new Students();
		stud5.setName("Somnath");
		stud5.setAge(29);
		stud5.setTeacher(teacher);

		RepositoryClass.saveStudent(stud5);

		Students stud6 = new Students();
		stud6.setName("Bubai");
		stud6.setAge(25);
		stud6.setTeacher(teacher);

		RepositoryClass.saveStudent(stud6);

		Students stud7 = new Students();
		stud7.setName("Piya");
		stud7.setAge(28);
		stud7.setTeacher(teacher);

		RepositoryClass.saveStudent(stud7);

//       List<Students> studList=new ArrayList<>();
//       
//       studList.add(stud1);
//       studList.add(stud2);
//       studList.add(stud3);
//       studList.add(stud4);
//       studList.add(stud5);
//       studList.add(stud6);
//       studList.add(stud7);

		teacher.setName("Ravish Sid");
		RepositoryClass.saveTeacher(teacher);

	}
}
